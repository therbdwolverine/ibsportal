package com.app.ibsportal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.app.ibsportal.models.FlightOffer;
import com.app.ibsportal.models.FlightOfferRequest;
import com.app.ibsportal.models.FlightOfferSegment;
import com.app.ibsportal.models.UserInfo;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.util.List;

public class FlightSearchResultActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    FlightOfferViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flight_search_result);

        FlightOfferRequest flightOfferRequest = (FlightOfferRequest) getIntent().getSerializableExtra("flightOfferRequest");
        UserInfo userInfo = (UserInfo) getIntent().getSerializableExtra("userInfo");

        TextView origin = findViewById(R.id.resultpage_origin);
        origin.setText(flightOfferRequest.getOriginAirportCode());

        TextView destination = findViewById(R.id.result_destination);
        destination.setText(flightOfferRequest.getDestinationAirportCode());
        TextView departingDate = findViewById(R.id.result_departuredate);
        try {
            departingDate.setText(flightOfferRequest.getFormattedReturnDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        recyclerView = (RecyclerView) findViewById(R.id.searchresult_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //TODO
        //Call the live service from here replacing the mock service calls


        MockFlightOfferSearchService flightOfferSearchService = new MockFlightOfferSearchService();
        List<FlightOffer> flightOffers = flightOfferSearchService.getMockFlightOffers();


        adapter = new FlightOfferViewAdapter(this, flightOffers, userInfo);
        recyclerView.setAdapter(adapter);
    }

}
