package com.app.ibsportal.models;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class FlightOffer implements Serializable {
    int airlineImage;
    List<FlightOfferSegment> segments;
    String totalCost;
    String totalScore;
    Map<String, String> factorScore; //Eg: Factor1, 9.0

    public FlightOffer(int image, List<FlightOfferSegment> segments, String totalCost, String totalScore,
                       Map<String, String> factorScore) {
        this.airlineImage=image;
        this.segments = segments;
        this.totalCost = totalCost;
        this.totalScore = totalScore;
        this.factorScore = factorScore;
    }

    public int getAirlineImage() {
        return airlineImage;
    }

    public void setAirlineImage(int airlineImage) {
        this.airlineImage = airlineImage;
    }

    public List<FlightOfferSegment> getSegments() {
        return segments;
    }

    public void setSegments(List<FlightOfferSegment> segments) {
        this.segments = segments;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public Map<String, String> getFactorScore() {
        return factorScore;
    }

    public void setFactorScore(Map<String, String> factorScore) {
        this.factorScore = factorScore;
    }
}
