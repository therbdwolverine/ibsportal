package com.app.ibsportal.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FlightOfferSegment implements Serializable {

    String originAirportCode;
    String destinationAirportCode;
    String startTime; //5:50PM
    String endTime; //11.45PM, if next day(11.45 (+1) )
    List<FlightOfferLeg> flightOfferLegs;
    String totalCost;
    String totalScore;
    Map<String, String> factorScore;
    String duration; //3h 2m
    String stops; //Non-Stop , 1 Stop, 2 Stops
    String airline; //Delta

    public FlightOfferSegment(String startTime, String endTime, String totalCost, String originAirportCode, String destinationAirportCode, List<FlightOfferLeg> flightOfferLegs, String totalScore, Map<String, String> factorScore, String duration, String stops, String airline) {
        this.totalCost = totalCost;
        this.originAirportCode = originAirportCode;
        this.destinationAirportCode = destinationAirportCode;
        this.flightOfferLegs = flightOfferLegs;
        this.totalScore = totalScore;
        this.factorScore = factorScore;
        this.duration = duration;
        this.stops = stops;
        this.airline = airline;
        this.startTime = startTime;
        this.endTime = endTime;
/*        List<String> airlines = new ArrayList<>();
        for (FlightOfferLeg flightOfferLeg: flightOfferLegs) {
           airlines.add(flightOfferLeg.getAirline());
        }

        this.airline = String.join(",", airlines);*/
    }

    public String getOriginAirportCode() {
        return originAirportCode;
    }

    public void setOriginAirportCode(String originAirportCode) {
        this.originAirportCode = originAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public void setDestinationAirportCode(String destinationAirportCode) {
        this.destinationAirportCode = destinationAirportCode;
    }

    public List<FlightOfferLeg> getFlightOfferLegs() {
        return flightOfferLegs;
    }

    public void setFlightOfferLegs(List<FlightOfferLeg> flightOfferLegs) {
        this.flightOfferLegs = flightOfferLegs;
    }

    public String getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(String totalScore) {
        this.totalScore = totalScore;
    }

    public Map<String, String> getFactorScore() {
        return factorScore;
    }

    public void setFactorScore(Map<String, String> factorScore) {
        this.factorScore = factorScore;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStops() {
        return stops;
    }

    public void setStops(String stops) {
        this.stops = stops;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getFormattedFromAndToAirportCode() {
        return originAirportCode + " &#x2192; " + destinationAirportCode;
    }

    public String getFormattedFromAndToTime() {
        return startTime + " &#x2192; " + endTime;
    }


 /*   public String getFormattedStopsMessageWithAirportCodes() {
        String airportCodes = "";
        if (!flightOfferLegs.isEmpty()) {
            airportCodes =  airportCodes.join(flightOfferLeg)
        }
        String airportCodes = "";


        for (FlightOfferLeg leg : flightOfferLegs ) {

        }

        return
    }*/
}
