package com.app.ibsportal.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FlightOfferRequest implements Serializable {
    String originAirportCode; //SEA
    String destinationAirportCode; //ORD
    String originDate; // MM/DD/YYYY
    String returnDate; // MM/DD/YYYY

    public FlightOfferRequest(String originAirportCode, String destinationAirportCode, String originDate, String returnDate) {
        this.originAirportCode = originAirportCode;
        this.destinationAirportCode = destinationAirportCode;
        this.originDate = originDate;
        this.returnDate = returnDate;
    }

    public String getOriginAirportCode() {
        return originAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public String getOriginDate() {
        return originDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getFormattedReturnDate() throws ParseException  {
        SimpleDateFormat inputFormatter = new SimpleDateFormat("MM/dd/yyyy");
        Date inputDate = inputFormatter.parse(originDate);
        SimpleDateFormat outputFormatter = new SimpleDateFormat("dd MMMM yyyy");
        return  outputFormatter.format(inputDate);
    }
}
