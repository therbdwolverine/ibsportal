package com.app.ibsportal.models;

import java.io.Serializable;
import java.util.List;

public class UserInfo implements Serializable {

    String id;
    String username;
    String password;
    List<String> ssrs;

    public UserInfo(String id, String username, String password, List<String> ssrs) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.ssrs = ssrs;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<String> getSsrs() {
        return ssrs;
    }

    public void setSsrs(List<String> ssrs) {
        this.ssrs = ssrs;
    }

    public String getFormattedText() {
        return "Hello " + id.substring(0,1).toUpperCase()+id.substring(1)+"!";
    }
}
