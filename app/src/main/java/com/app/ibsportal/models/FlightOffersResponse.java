package com.app.ibsportal.models;

import java.io.Serializable;
import java.util.List;

public class FlightOffersResponse implements Serializable {
    List<FlightOffer> flightOfferList;

    public List<FlightOffer> getFlightOfferList() {
        return flightOfferList;
    }

    public void setFlightOfferList(List<FlightOffer> flightOfferList) {
        this.flightOfferList = flightOfferList;
    }
}
