package com.app.ibsportal.models;

import java.io.Serializable;

public class FlightOfferLeg implements Serializable {
    String originAirportCode; //SEA
    String destinationAirportCode; //MSP
    String duration; // 23h 14m
    String airline; //Delta

    public FlightOfferLeg(String originAirportCode, String destinationAirportCode, String duration, String airline) {
        this.originAirportCode = originAirportCode;
        this.destinationAirportCode = destinationAirportCode;
        this.duration = duration;
        this.airline = airline;
    }

    public String getOriginAirportCode() {
        return originAirportCode;
    }

    public void setOriginAirportCode(String originAirportCode) {
        this.originAirportCode = originAirportCode;
    }

    public String getDestinationAirportCode() {
        return destinationAirportCode;
    }

    public void setDestinationAirportCode(String destinationAirportCode) {
        this.destinationAirportCode = destinationAirportCode;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }
}
