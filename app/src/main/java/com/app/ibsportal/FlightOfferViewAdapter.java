package com.app.ibsportal;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.transition.Visibility;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.ibsportal.models.FlightOffer;
import com.app.ibsportal.models.FlightOfferSegment;
import com.app.ibsportal.models.UserInfo;

import java.util.List;

public class FlightOfferViewAdapter extends RecyclerView.Adapter<FlightOfferViewAdapter.FlightOfferViewHolder>{

    private Context context;
    private List<FlightOffer> flightOffers;
    private UserInfo userInfo;

    public FlightOfferViewAdapter(Context context, List<FlightOffer> flightOffers, UserInfo userInfo) {
        this.context = context;
        this.flightOffers = flightOffers;
        this.userInfo = userInfo;
    }

    @NonNull
    @Override
    public FlightOfferViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.flightsofferlayout, null );
        FlightOfferViewHolder holder = new FlightOfferViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull FlightOfferViewHolder holder, int i) {
        final FlightOffer offer = flightOffers.get(i);
        FlightOfferSegment firstOfferSegment = offer.getSegments().get(0);

        holder.airlineLogoImage.setImageDrawable(context.getResources()
                .getDrawable(offer.getAirlineImage()));
        holder.firstSegmentTime.setText(firstOfferSegment.getStartTime() + "-" + firstOfferSegment.getEndTime());
        holder.firstSegmentFromAndTo.setText(firstOfferSegment.getOriginAirportCode() + "-" +
                firstOfferSegment.getDestinationAirportCode());
        holder.firstAirlines.setText(firstOfferSegment.getAirline());

        if (offer.getSegments().size() > 1) {
            holder.secondSegmentLayout.setVisibility(View.VISIBLE);
            FlightOfferSegment secondOfferSegment = offer.getSegments().get(1);
            holder.secondSegmentTime.setText(secondOfferSegment.getStartTime() + "-" + secondOfferSegment.getEndTime());
            holder.secondSegmentFromAndTo.setText(secondOfferSegment.getOriginAirportCode()
                    + "-" + secondOfferSegment.getDestinationAirportCode());
            holder.secondAirlines.setText(secondOfferSegment.getAirline());
        } else {
            holder.secondSegmentLayout.setVisibility(View.GONE);
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(context, CheckoutActivity.class);
                mainIntent.putExtra("flightOffer", offer);
                mainIntent.putExtra("userInfo", userInfo);
                context.startActivity(mainIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return flightOffers.size();
    }

    class FlightOfferViewHolder extends RecyclerView.ViewHolder  {

        ImageView airlineLogoImage;
        ImageView disabilityImage;
        TextView firstSegmentTime,firstSegmentFromAndTo, firstAirlines, secondSegmentTime,
                secondSegmentFromAndTo, secondAirlines, price;
        CardView cardView;
        LinearLayout secondSegmentLayout;

        public FlightOfferViewHolder(@NonNull View view) {
            super(view);

            airlineLogoImage = (ImageView) view.findViewById(R.id.fightofferview_logo);

            firstSegmentTime = (TextView) view.findViewById(R.id.fightofferview_firstSegmentTime);
            firstSegmentFromAndTo = (TextView) view.findViewById(R.id.fightofferview_fromAndToDestination);
            firstAirlines = (TextView) view.findViewById(R.id.fightofferview_airlines);

            secondSegmentTime = (TextView) view.findViewById(R.id.fightofferview_secondSegmentTime);
            secondSegmentFromAndTo = (TextView) view.findViewById(R.id.fightofferview_secondSegment_fromAndToDestination);
            secondAirlines = (TextView) view.findViewById(R.id.fightofferview_secondSegment_airlines);


            price = (TextView) view.findViewById(R.id.flightofferview_price);
            disabilityImage = (ImageView) view.findViewById(R.id.fightofferview_disabilityimg);
            cardView = (CardView) view.findViewById(R.id.flightofferview_cardview);

            secondSegmentLayout = (LinearLayout) view.findViewById(R.id.fightofferview_secondSegment_1);
        }
    }
}
