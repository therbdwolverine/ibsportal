package com.app.ibsportal;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class UberActivity extends AppCompatActivity {

    private GifLoadingViewWrapper mGifLoadingView = new GifLoadingViewWrapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uber);

        WebView webView = findViewById(R.id.gifImageView);
        webView.loadUrl("file:///android_asset/uber.html");


        Button btn_requestuber = findViewById(R.id.book_uber_button);

        btn_requestuber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGifLoadingView.setImageResource(R.drawable.num0);

                mGifLoadingView.show(getFragmentManager());

                //TODO Shyam service
                invokeUberService();
            }
        });






    }

    private void invokeUberService() {

    }

}
