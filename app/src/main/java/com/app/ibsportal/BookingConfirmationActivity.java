package com.app.ibsportal;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ibsportal.models.UserInfo;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class BookingConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_confirmation);
        ImageView qrCode = findViewById(R.id.barcode);
        TextView username = findViewById(R.id.username);
        TextView itinNumberView = findViewById(R.id.itinNumber);
        TextView totalAmount = findViewById(R.id.totalAmount);
        Button doneButton = findViewById(R.id.done);

        final UserInfo userInfo = new UserInfo("ryan", "ryan@cronos.com", "123456", null);
        final String itinNumber = "OD54312";
        //UserInfo userInfo = (UserInfo) getIntent().getSerializableExtra("userInfo");
        username.setText("Hello " + userInfo.getId());
        itinNumberView.setText("Booking # : " + itinNumber);
        totalAmount.setText("Booking Amount : " + "$350");

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(itinNumber, BarcodeFormat.QR_CODE,200,200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            qrCode.setImageBitmap(bitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent searchIntent = new Intent(BookingConfirmationActivity.this,
                        SearchAcitvity.class);
                searchIntent.putExtra("userInfo", userInfo);
                startActivity(searchIntent);
                finish();
            }
        });
    }
}
