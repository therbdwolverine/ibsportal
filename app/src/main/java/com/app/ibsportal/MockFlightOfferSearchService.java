package com.app.ibsportal;

import com.app.ibsportal.models.FlightOffer;
import com.app.ibsportal.models.FlightOfferLeg;
import com.app.ibsportal.models.FlightOfferSegment;
import com.app.ibsportal.models.FlightOffersResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MockFlightOfferSearchService {

    public FlightOffersResponse getMockResponse() {
        FlightOffersResponse response = new FlightOffersResponse();
        response.setFlightOfferList(getMockFlightOffers());
        return response;
    }

    public List<FlightOffer> getMockFlightOffers() {
        List<FlightOffer> flightOffers = new ArrayList<FlightOffer>();

        FlightOffer offer1 = new FlightOffer(R.drawable.cronoslogo, getOffer1_Segments(), "300.0", "8.0", factorScores());
        FlightOffer offer2 = new FlightOffer(R.drawable.cronoslogo, getOffer2_Segments(), "400.0", "8.0", factorScores());

        flightOffers.add(offer1);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);
        flightOffers.add(offer2);


        return flightOffers;
    }


    public List<FlightOfferSegment> getOffer1_Segments() {
       List<FlightOfferLeg> offer1_Segment1_Legs = getOffer1_Segment1_Legs();
        List<FlightOfferLeg> offer1_Segment2_Legs = getOffer1_Segment2_Legs();

       List<FlightOfferSegment> segments = new ArrayList<>();
       segments.add(new FlightOfferSegment("8:45","10:45","192.0","SEA", "ORD",
               offer1_Segment1_Legs, "9.0", factorScores(), "3h 20m", "1 stop", "Cronos"));

       //Uncomment to add return segment
       /*segments.add(new FlightOfferSegment("8:45","10:45","210.0","ORD", "SEA",
               offer1_Segment2_Legs, "7.0", factorScores(), "3h 20m", "1 stop", "American"));
 */       return segments;
    }

    public List<FlightOfferSegment> getOffer2_Segments() {
        List<FlightOfferLeg> offer2_Segment1_Legs = getOffer2_Segment1_Legs();
        List<FlightOfferLeg> offer2_Segment2_Legs = getOffer2_Segment2_Legs();
        List<FlightOfferSegment> segments = new ArrayList<>();
        segments.add(new FlightOfferSegment("8:45","10:45","190.0","COK", "LCY",
                offer2_Segment1_Legs, "9.0", factorScores(), "3h 20m", "1 stop", "Cronos"));
        /*segments.add(new FlightOfferSegment("8:45","10:45","210.0","ORD", "SEA",
                offer2_Segment2_Legs, "7.0", factorScores(), "3h 20m", "1 stop", "American"));
  */      return segments;
    }

    public List<FlightOfferLeg> getOffer1_Segment1_Legs() {
        List<FlightOfferLeg> legs = new ArrayList<>();
        legs.add(new FlightOfferLeg("SEA", "MSP", "1h 20m", "Cronos"));
        legs.add(new FlightOfferLeg("MSP", "ORD", "1h 00m", "Cronos"));
        return legs;
    }

    public List<FlightOfferLeg> getOffer1_Segment2_Legs() {
        List<FlightOfferLeg> legs = new ArrayList<>();
        legs.add(new FlightOfferLeg("ORD", "MSP", "1h 20m", "American"));
        legs.add(new FlightOfferLeg("MSP", "SEA", "1h 00m", "American"));
        return legs;
    }

    public List<FlightOfferLeg> getOffer2_Segment1_Legs() {
        List<FlightOfferLeg> legs = new ArrayList<>();
        legs.add(new FlightOfferLeg("COK", "LCY", "1h 20m", "Cronos"));
        legs.add(new FlightOfferLeg("LCY", "COK", "1h 00m", "Cronos"));
        return legs;
    }

    public List<FlightOfferLeg> getOffer2_Segment2_Legs() {
        List<FlightOfferLeg> legs = new ArrayList<>();
        legs.add(new FlightOfferLeg("ORD", "MSP", "1h 20m", "American"));
        legs.add(new FlightOfferLeg("MSP", "SEA", "1h 00m", "American"));
        return legs;
    }

    public Map<String, String> factorScores() {
        Map<String, String> factorScores = new HashMap<>();
        factorScores.put("Equipment", "9.0");
        factorScores.put("Weather", "7.0");
        return factorScores;
    }
 }
