package com.app.ibsportal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.app.ibsportal.models.UserInfo;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button button = findViewById(R.id.loginactivity_btn_login);

        final UserInfo userInfo = new UserInfo("ryan", "ryan@cronos.com", "123456", null);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mainIntent = new Intent(LoginActivity.this, SearchAcitvity.class);
                mainIntent.putExtra("userInfo", userInfo);
                startActivity(mainIntent);
                finish();
            }
        });
    }

}
