package com.app.ibsportal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.ibsportal.models.FlightOfferRequest;
import com.app.ibsportal.models.UserInfo;

import java.util.Calendar;

public class SearchAcitvity extends AppCompatActivity {

    private GifLoadingViewWrapper mGifLoadingView = new GifLoadingViewWrapper();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_acitvity);

        final UserInfo userInfo = (UserInfo) getIntent().getSerializableExtra("userInfo");

        TextView helloText = findViewById(R.id.searchactivity_hellotext);
        helloText.setText(userInfo.getFormattedText());

        String[] airports = { "Seattle / Tacoma International, WA (SEA)",
                "Chicago O'Hare International, IL (ORD)",
                "New York John F. Kennedy, NY (JFK)",
                "Washington Ronald Reagan, VA (DCA)", "Heathrow Airport, (LHR)"};

        ImageView btn_profile = findViewById(R.id.search_profile);

        btn_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent profile_intent = new Intent(SearchAcitvity.this,
                        ProfileActivity.class);
                startActivity(profile_intent);
                finish();
            }
        });

        ImageView btn_uber_assist = findViewById(R.id.search_uber);

        btn_uber_assist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent profile_intent = new Intent(SearchAcitvity.this,
                        ProfileActivity.class);
                startActivity(profile_intent);
                finish();
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this,android.R.layout.select_dialog_item, airports);

        final AutoCompleteTextView txt_origin = (AutoCompleteTextView)
                findViewById(R.id.searchactivity_origin_et);
        txt_origin.setThreshold(2);
        txt_origin.setAdapter(adapter);

        final AutoCompleteTextView txt_dest = (AutoCompleteTextView)
                findViewById(R.id.searchactivity_destination_et);
        txt_dest.setThreshold(2);
        txt_dest.setAdapter(adapter);

        final EditText et_departingDate = (EditText) findViewById(R.id.searchactivity_departing_et);
        et_departingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(SearchAcitvity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        et_departingDate.setText(selectedmonth + "/" + selectedday + "/" + selectedyear);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Departing on");
                mDatePicker.show();  }

        });


        final EditText et_returningDate =  findViewById(R.id.searchactivity_returning_et);
        et_returningDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(SearchAcitvity.this, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        et_returningDate.setText(selectedmonth + "/" + selectedday + "/" + selectedyear);
                    }
                },mYear, mMonth, mDay);
                mDatePicker.setTitle("Returning on");
                mDatePicker.show();  }
        });

        Button goButton = findViewById(R.id.searchactivity_go_button);
        goButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGifLoadingView.setImageResource(R.drawable.num0);

                mGifLoadingView.show(getFragmentManager());
                if(txt_origin.getText() != null && txt_origin.getText().toString().length() > 0
                        && txt_dest.getText() != null && txt_dest.getText().toString().length() > 0
                        && et_departingDate.getText() != null){

                    String origin_et = txt_origin.getText().toString();
                    String origin =  (origin_et.contains("(")) ? origin_et.substring(origin_et.indexOf("(")+1, origin_et.indexOf(")")) : origin_et.toUpperCase();
                    String dest_et = txt_dest.getText().toString();
                    String destination = (dest_et.contains("(")) ?
                            dest_et.substring(dest_et.indexOf("(")+1, dest_et.indexOf(")")) : dest_et.toUpperCase();

                    String departingDate = et_departingDate.getText() != null ? et_departingDate.getText().toString() : "";
                    String returningDate = et_returningDate.getText() != null ? et_returningDate.getText().toString() : "";

                    final Intent mainIntent = new Intent(SearchAcitvity.this,
                            FlightSearchResultActivity.class);

                    FlightOfferRequest request = new FlightOfferRequest(origin, destination, departingDate, returningDate);

                    mainIntent.putExtra("flightOfferRequest", request);
                    mainIntent.putExtra("userInfo", userInfo);


                    System.out.println("origin:"+origin +
                            "\ndest:"+destination+
                            "\ndepartingDate:"+ departingDate +
                            "\nreturningDate:"+ returningDate);

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            mGifLoadingView.dismiss();
                            startActivity(mainIntent);
                            finish();
                        }
                    }, 3000);


                } else {
                    System.out.println("Missing inputs");
                }

            }
        });
    }

}
